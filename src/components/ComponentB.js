import CompD from './ComponentD';
import CounterContext from './context/Counter';

const CompB = () => {


    return(
        <CounterContext.Consumer>
            {(counter) => (
                <div>
                    <h2>Component B</h2>
                    <h2>{counter.count}</h2>
                    <CompD />
                </div>
            )}
        </CounterContext.Consumer>
    )
}

export default CompB; 