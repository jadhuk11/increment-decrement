import CounterContext from './context/Counter';

const CompC = () => {  
    
    return(
        <CounterContext.Consumer>
            {(counter) => (
                <div>
                    <h2>Component C</h2>
                    <h2>{counter.count}</h2>
                    <button onClick={counter.increment}>tambah</button>
                    <button onClick={counter.decrement}>kurang</button>
                </div>
            )}
        </CounterContext.Consumer>
    )
}

export default CompC;