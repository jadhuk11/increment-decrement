import React, { useState } from 'react';
import CompB from './components/ComponentB';
import CompC from './components/ComponentC';
import CounterContext from './components/context/Counter';

const Counter = () => {
    const [ count, setCounter ] = useState(0);

    const increment = () => {
        setCounter(count + 1);
    }
    
    const decrement = () => {
        if (count > 0) {
            setCounter(count - 1);
        }
    }    

return (
    <CounterContext.Provider value={{count, increment, decrement}}>
        <h2>Component A </h2>
        <CompB />
        <CompC />
    </CounterContext.Provider>           
    );
};

export default Counter;


